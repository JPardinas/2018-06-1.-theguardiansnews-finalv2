Project Overview

The final project is a chance for you to combine and practice everything you learned in this section of the Nanodegree program. 
You will be making your own app that connects to the Internet to provide news articles on a topic of your choice.

You are expected to have passed the News App, Stage 1 project prior to beginning this project.

The goal is to add a Settings Screen to the News Feed app you made earlier in Part 1 which will allow users to narrow down the 
stories displayed in from the feed. The available preference options presented to the user will be left up to you to decide 
(e.g. publishing time, country, topic category, etc. )