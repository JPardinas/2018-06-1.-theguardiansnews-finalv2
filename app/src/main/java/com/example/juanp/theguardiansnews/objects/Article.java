package com.example.juanp.theguardiansnews.objects;

public class Article {

    //Declaration of the variables
    private String webTitle;
    private String sectionName;
    private String webPublicationDate;
    private String webUrl;
    private String author;
    private String thumbnail;


    //Constructor
    public Article(String webTitle, String sectionName, String webPublicationDate, String webUrl, String author, String thumbnail) {
        this.webTitle = webTitle;
        this.sectionName = sectionName;
        this.webPublicationDate = webPublicationDate;
        this.webUrl = webUrl;
        this.author = author;
        this.thumbnail = thumbnail;
    }

    //Getter
    public String getWebTitle() {
        return webTitle;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getWebPublicationDate() {
        return webPublicationDate;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getAuthor() {
        return author;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
