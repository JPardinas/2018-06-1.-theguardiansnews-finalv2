package com.example.juanp.theguardiansnews.activity;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.juanp.theguardiansnews.adapter.ArticleAdapter;
import com.example.juanp.theguardiansnews.loader.ArticleLoader;
import com.example.juanp.theguardiansnews.objects.Article;
import com.example.juanp.theguardiansnews.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LoaderManager.LoaderCallbacks<List<Article>> {

    //ArticleAdapter and loader id
    private static final int ARTICLE_LOADER_ID = 1;

    //Initial string with search parametres
    private static final String JSON_INITIAL_LINK = "https://content.guardianapis.com/search?";

    //JSON link with the final query
    private static String JSON_LINK = "";

    //Adapter
    private ArticleAdapter mAdapter;

    //Textview if there are no items
    private TextView textNoArticles;

    //Boolean to check if it is the first start of the app, to call restartLoader or initLoader
    private boolean firstStartCheck = false;

    //
    private int currentSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Adapter and ListView for the items of the array
        mAdapter = new ArticleAdapter(this, new ArrayList<Article>());
        ListView articleListView = (ListView) findViewById(R.id.listArticles);
        articleListView.setAdapter(mAdapter);

        //Textview if there are no items
        textNoArticles = (TextView) findViewById(R.id.textNoConnection);

        //OnclickListener to each item -> sends an intent to open the Article in a web browser
        articleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Article currentArticle = mAdapter.getItem(position);
                Uri articleUrl = Uri.parse(currentArticle.getWebUrl());

                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, articleUrl);

                startActivity(websiteIntent);
            }
        });

        //Quit tint from the navigationview
        navigationView.setItemIconTintList(null);
        //Starts application with the opinions section
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
    }

    //Create the loader with the JSON Link
    @Override
    public Loader<List<Article>> onCreateLoader(int i, Bundle bundle) {
        return new ArticleLoader(this, JSON_LINK, currentSection);
    }

    //Call after the loader finish
    @Override
    public void onLoadFinished(Loader<List<Article>> loader, List<Article> articles) {
        // Hide loading indicator because the data has been loaded
        View loadingIndicator = findViewById(R.id.progressBarMain);
        loadingIndicator.setVisibility(View.GONE);

        // Set the text if there are not articles
        textNoArticles.setText(R.string.no_articles);

        // Call clear method to the adapter
        mAdapter.clear();

        //If there are articles send the array to the adapter and hide the empty text view
        if (articles != null && !articles.isEmpty()) {
            mAdapter.addAll(articles);
            textNoArticles.setVisibility(View.GONE);
        }
    }

    // Calls loaderReset clears the adapter
    @Override
    public void onLoaderReset(Loader<List<Article>> loader) {
        mAdapter.clear();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        //News button
        if (id == R.id.nav_news) {
            currentSection = 0;
            updateJson(currentSection);
            callLoader();

            //Sport button
        } else if (id == R.id.nav_sport) {
            currentSection = 1;
            updateJson(currentSection);
            callLoader();

            //Culture button
        } else if (id == R.id.nav_culture) {
            currentSection = 2;
            updateJson(currentSection);
            callLoader();

            //Fashion button
        } else if (id == R.id.nav_fashion) {
            currentSection = 3;
            updateJson(currentSection);
            callLoader();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void updateJson(int currentSection) {
        //Get the preferences and update de JSON_LINK
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //Get page size preference
        String pageSize = sharedPreferences.getString(getString(R.string.settings_page_size_key),getString(R.string.settings_page_size_default_value));

        // Get search query preference
        String searchInput = sharedPreferences.getString(getString(R.string.settings_search_query_user), getString(R.string.settings_search_query_default));

        // Get order by preference
        String orderBy = sharedPreferences.getString(getString(R.string.settings_order_by_list_key), getString(R.string.settings_order_by_list_default));

        // Build the Uri based on the preferences
        Uri baseUri = Uri.parse(JSON_INITIAL_LINK);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("page-size", pageSize);
        uriBuilder.appendQueryParameter("order-by", orderBy);
        uriBuilder.appendQueryParameter("show-tags", "contributor");

        //Switch to append the section
        switch (currentSection){
            case 0:
                uriBuilder.appendQueryParameter("section","news");
                break;
            case 1:
                uriBuilder.appendQueryParameter("section","sport");
                break;
            case 2:
                uriBuilder.appendQueryParameter("section","culture");
                break;
            case 3:
                uriBuilder.appendQueryParameter("section","fashion");
                break;
        }

        uriBuilder.appendQueryParameter("show-fields", "thumbnail");
        uriBuilder.appendQueryParameter("q", searchInput);
        uriBuilder.appendQueryParameter("api-key", "cbaed1ef-2de5-4734-a3eb-9477cb66d5f1");
        JSON_LINK = uriBuilder.toString();
    }

    //Methdd to call the loader
    private void callLoader() {

        //Set visible the loadingIndicator
        View loadingIndicator = findViewById(R.id.progressBarMain);
        loadingIndicator.setVisibility(View.VISIBLE);
        // Check connectivity
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get network info
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // If there are connection call the loader
        if (networkInfo != null && networkInfo.isConnected()) {
            LoaderManager loaderManager = getLoaderManager();
            if (firstStartCheck) {
                loaderManager.restartLoader(ARTICLE_LOADER_ID, null, this);
            } else {
                firstStartCheck = true;
                loaderManager.initLoader(ARTICLE_LOADER_ID, null, this);
            }
        } else {
            loadingIndicator.setVisibility(View.GONE);
            textNoArticles.setText(R.string.no_internet_connection);
        }
    }
}
