package com.example.juanp.theguardiansnews.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.juanp.theguardiansnews.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity_settings);
    }

    // Fragment with the preferences
    public static class NewsPreferenceFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Set preferences resource
            addPreferencesFromResource(R.xml.settings_main);
            //Number of pages
            Preference pageSize = findPreference(getString(R.string.settings_page_size_key));
            bindPreferenceToValue(pageSize);
            // Search Query
            Preference searchInput = findPreference(getString(R.string.settings_search_query_user));
            bindPreferenceToValue(searchInput);
            // Order by
            Preference orderBy = findPreference(getString(R.string.settings_order_by_list_key));
            bindPreferenceToValue(orderBy);
        }

        private void bindPreferenceToValue(Preference preference) {
            // OnPreferenceChangeListener
            preference.setOnPreferenceChangeListener(this);
            // Get SharedPreferences
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences
                    (preference.getContext());
            // Get the default value
            String preferenceString = sharedPreferences.getString(preference.getKey(), "");
            // Show the value
            onPreferenceChange(preference, preferenceString);
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // Store new value
            String stringValue = newValue.toString();
            // If this is a list preference, use values instead of keys to update summary
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int prefIndex = listPreference.findIndexOfValue(stringValue);
                if (prefIndex >= 0) {
                    CharSequence[] labels = listPreference.getEntries();
                    preference.setSummary(labels[prefIndex]);
                }
            }
            // Set new value
            preference.setSummary(stringValue);
            return true;
        }
    }
}
