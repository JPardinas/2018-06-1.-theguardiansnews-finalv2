package com.example.juanp.theguardiansnews.utils;

import android.text.TextUtils;
import android.util.Log;

import com.example.juanp.theguardiansnews.objects.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class QueryUtils {

    private static final String LOG_TAG = QueryUtils.class.getSimpleName();

    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {
    }

    public static List<Article> fetchArticleData(String requestUrl, int currentSection) {
        // Create URL object
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
        }

        // Extract relevant fields from the JSON response and create a list of {@link Article}s
        List<Article> articles = extractArticle(jsonResponse, currentSection);

        // Return the list of {@link articles}s
        return articles;
    }

    /**
     * Returns new URL object from the given string URL.
     */
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Problem building the URL ", e);
        }
        return url;
    }

    /**
     * Make an HTTP request to the given URL and return a String as the response.
     */
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the articles JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                // Closing the input stream could throw an IOException, which is why
                // the makeHttpRequest(URL url) method signature specifies than an IOException
                // could be thrown.
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    /**
     * Convert the {@link InputStream} into a String which contains the
     * whole JSON response from the server.
     */
    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static List<Article> extractArticle(String articlesJson, int currentSection) {

        if (TextUtils.isEmpty(articlesJson)) {
            return null;
        }

        // Create an empty ArrayList that we can start adding earthquakes to
        List<Article> articles = new ArrayList<>();

        // Try to parse the response
        try {
            // Create JSON Objects and arrays
            JSONObject baseJsonResponse = new JSONObject(articlesJson);
            JSONObject responseObject = baseJsonResponse.getJSONObject("response");
            JSONArray articlesArray = responseObject.getJSONArray("results");

            //Bucle for to get all the info that we want
            for (int i = 0; i < articlesArray.length(); i++) {

                //JSON object and array to get all the info
                JSONObject currentArticle = articlesArray.getJSONObject(i);
                JSONArray tagsArray = currentArticle.getJSONArray("tags");

                //Strings with the info
                String webTitle = currentArticle.getString("webTitle");
                String sectionName = currentArticle.getString("sectionName");
                String webPublicationDate = currentArticle.getString("webPublicationDate");
                String webUrl = currentArticle.getString("webUrl");
                String author ="";
                String thumbnail;

                //Try - catch to get the image of the articles (some articles don't have this field)
                try{
                    JSONObject fields = currentArticle.getJSONObject("fields");
                    thumbnail = fields.getString("thumbnail");
                }catch (JSONException e){
                    //If there is not thumbnail resource set a default image for each section
                    switch (currentSection){
                        case 0:
                            thumbnail = "http://www.europeanea.org/wp-content/uploads/2016/03/news.jpg";
                            break;
                        case 1:
                            thumbnail = "https://www.muralesyvinilos.com/murales/metacrilato/deportes_muralesyvinilos_11280280__Monthly_XXL.jpg";
                            break;
                        case 2:
                            thumbnail = "https://etimes.com.ng/wp-content/uploads/2017/11/240_F_96906416_lQc2sr0YTrTmhW7sGIeQbXRWDAsAzZ6b.jpg";
                            break;
                        case 3:
                            thumbnail = "http://1.bp.blogspot.com/-QDUaT8SO5N8/VOdWijKMBPI/AAAAAAAACtk/A-JIUVyi018/s1600/fashion-news.png";
                            break;
                        default:
                            thumbnail = "http://www.europeanea.org/wp-content/uploads/2016/03/news.jpg";
                            break;
                    }
                }

                //Nested for bucle for to get the authors in the json data
                for (int j = 0; j < tagsArray.length(); j++) {
                    JSONObject currentAuthor = tagsArray.getJSONObject(j);
                    author = currentAuthor.getString("webTitle");
                }

                //Create article object with the getted info and add to the list
                Article article = new Article(webTitle, sectionName, webPublicationDate, webUrl, author, thumbnail);
                articles.add(article);
            }

            // Catch to handle exception
        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the articles JSON results", e);
        }
        return articles;
    }
}

